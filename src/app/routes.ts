import { HomeComponent } from "./home/home.component";
import { ProfileComponent } from "./user/profile.component";
import { ProgramDetailComponent } from "./program/program-detail.component";
import { Routes } from "@angular/router";
import { ProgramComponent } from "./program/program.component";
import { RouteResolver } from './service/route.resolver';
import { SignInComponent } from './signin/signin.component';
import { LoggedInGuard } from './service/login.guard';

export const appRoutes: Routes = [
    { path: "", redirectTo: '/home', pathMatch: 'full' },
    { path: 'signin', component: SignInComponent },
    { path: "home", component: HomeComponent, resolve: { data: RouteResolver }, canActivate: [LoggedInGuard] },
    {
        path: "programs", component: ProgramComponent,resolve: { data: RouteResolver },
        children: [
            { path: 'programDetail/:program', component: ProgramDetailComponent },
            { path: 'profile', component: ProfileComponent }
        ]
    },
    { path: '**', component: HomeComponent }
];