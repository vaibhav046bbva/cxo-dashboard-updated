import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component, APP_INITIALIZER } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { ProgramComponent } from './program/program.component';
import { ProgramDetailComponent } from './program/program-detail.component'
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './user/profile.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './routes';
import { GapiSession } from './service/gapi.session';
import { SignInComponent } from './signin/signin.component';
import { FooterComponent } from './common/footer.component';
import { HeaderComponent } from './common/header.component';
import { HttpClientModule } from '@angular/common/http';
import { BackgroundImageComponent } from './common/backgroundImage.component';
import { RouteResolver } from './service/route.resolver';
import { NguCarouselModule } from '@ngu/carousel';
import { ProgramCarouselComponent } from './common/carousel.component';
import { LoggedInGuard } from './service/login.guard';
import { AmountSuffixPipe } from './pipes/custom.format';

export function initGapi(gapiSession: GapiSession) {
  return () => gapiSession.initClient();
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    SignInComponent,
    AmountSuffixPipe,
    ProfileComponent,
    BackgroundImageComponent,
    ProgramComponent,
    ProgramCarouselComponent,
    ProgramDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    NguCarouselModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    { provide: APP_INITIALIZER, useFactory: initGapi, deps: [GapiSession], multi: true },
    GapiSession,
    RouteResolver,
    LoggedInGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
