import { Component, OnInit } from '@angular/core';
import { NguCarouselConfig } from '@ngu/carousel';
 
@Component({
  selector: 'programCarousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class ProgramCarouselComponent implements OnInit {
  imgags = [
    'assets/bg.jpg',
    'assets/car.png',
    'assets/canberra.jpg',
    'assets/holi.jpg'
  ];
  public carouselTileItems: Array<any> = [{
    class: 'program-1',
    imgSrc: '../../assets/img/micro-illustration-be.png',
  }, {
    class: 'program-2',
    imgSrc: '../../assets/img/micro-illustration-be.png',
  }, {
    class: 'program-3',
    imgSrc: '../../assets/img/typography-graphic.png',
  }, {
    class: 'program-4',
    imgSrc: '../../assets/img/micro-illustration-be.png',
  }, {
    class: 'program-5',
    imgSrc: '../../assets/img/photography-be.png',
  }];
  public carouselTiles = {
    0: [],
    1: [],
    2: [],
    3: [],
    4: [],
    5: []
  };
  public carouselTile: NguCarouselConfig = {
    grid: { xs: 1, sm: 2, md: 3, lg: 3, all: 0 },
    slide: 1,
    speed: 250,
    point: {
      visible: true
    },
    load: 2,
    velocity: 0,
    touch: true,
    loop: true,
    easing: 'cubic-bezier(0, 0, 0.2, 1)'
  };
  constructor() {}
 
  ngOnInit() {
    // this.carouselTileItems.forEach(el => {
    //   this.carouselTileLoad(el.value);
    // });
  }
 
  public carouselTileLoad(j) {
    // console.log(this.carouselTiles[j]);
    const len = this.carouselTiles[j].length;
    if (len <= 30) {
      for (let i = len; i < len + 15; i++) {
        this.carouselTiles[j].push(
          this.imgags[Math.floor(Math.random() * this.imgags.length)]
        );
      }
    }
  }
}