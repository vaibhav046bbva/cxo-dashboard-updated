import { Component, OnInit, AfterViewInit } from '@angular/core';

//import { Navigation } from 'starter-kit';
@Component({
    selector: 'app-header-component',
    templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, AfterViewInit {
    // private navigation = Navigation.init({ initSelector: '.navigation' });
    // private navItems: String[] = ["Home", "Program"];
    header = null;
    sticky = null;
    mobileWidth = 960; //add classs sticky for mobile header
    ismenuOpend = false;

    constructor() {

    }

    ngOnInit() {
        //this.navigation.Entries = this.navItems;
        //console.log(this.navigation);
      let ismenuOpend = false;
    }
    addScrollEvent() {
      /*  if (window.pageYOffset > this.sticky && window.innerWidth < this.mobileWidth) {
            this.header.classList.remove("transparent-background");
            this.header.classList.add("sticky");
        } else {
            this.header.classList.remove("sticky");
            this.header.classList.add("transparent-background");
        }*/
    }
    ngAfterViewInit() {
        //window.onscroll = () => {this.addScrollEvent()};
       // this.header = document.getElementById("app_header");
       // this.sticky = this.header.offsetTop;
    }
    toogleNav(evt) {

        evt.preventDefault();
        this.ismenuOpend = !this.ismenuOpend;
       // let element = document.getElementById('mobile_menu');
        //element.classList.toggle('active');
    }

}
