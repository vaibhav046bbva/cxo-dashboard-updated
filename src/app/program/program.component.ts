 import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { TableDataModel } from '../models/tableData.model';
import { ResultModel } from '../models/result.model';
import { Constants } from '../utils/constants';
import { Calculate } from '../utils/calculate';

@Component({
  selector: 'app-program-component',
  templateUrl: './program.component.html'
})
export class ProgramComponent implements OnInit {
  result: ResultModel = ResultModel.getInstance();
  private calculate: Calculate = new Calculate();
  private tableData: Object = {
    headers: [{
      title: 'PROGRAM',
      subTitle: ''
    }, {
      title: 'ORG',
      subTitle: ''
    }, {
      title: 'PROGRAM SPEND',
      subTitle: ''
    }, {
      title: 'PROGRAM SAVING',
      subTitle: ''
    }, {
      title: 'NO. OF RESOURCES',
      subTitle: 'ONSHORE'
    }, {
      title: 'NO. OF RESOURCES',
      subTitle: 'OFFSHORE'
    }],
    "data": [],
    "totals": {}
  }
  private data: TableDataModel[] = [];

  private onshore: Object[] = [{
    name: "Sofia Gracia",
    designation: "Department Specific Title",
    imageUrl: "/assets/img/girl-profile.png"
  }, {
    name: "Daniela Fernandez",
    designation: "Department Specific Title",
    imageUrl: "/assets/img/Default_Team_Member.png"
  }, {
    name: "Maria Gonzalez",
    designation: "Department Specific Title",
    imageUrl: "/assets/img/Default_Team_Member.png"
  }, {
    name: "Juan Rodriguez",
    designation: "Department Specific Title",
    imageUrl: "/assets/img/Default_Team_Member.png"
  }, {
    name: "Rene Lopez",
    designation: "Department Specific Title",
    imageUrl: "/assets/img/girl-profile.png"
  }, {
    name: "Marta Martinz",
    designation: "Department Specific Title",
    imageUrl: "/assets/img/Default_Team_Member.png"
  }, {
    name: "Julio Sanchez",
    designation: "Department Specific Title",
    imageUrl: "/assets/img/girl-profile.png"
  }, {
    name: "Pedro Perez",
    designation: "Department Specific Title",
    imageUrl: "/assets/img/Default_Team_Member.png"
  }];

  private offshore: Object[] = [{
    name: "Sofia Gracia",
    designation: "Department Specific Title",
    imageUrl: "/assets/img/girl-profile.png"
  }, {
    name: "Daniela Fernandez",
    designation: "Department Specific Title",
    imageUrl: "/assets/img/Default_Team_Member.png"
  }, {
    name: "Maria Gonzalez",
    designation: "Department Specific Title",
    imageUrl: "/assets/img/girl-profile.png"
  }, {
    name: "Juan Rodriguez",
    designation: "Department Specific Title",
    imageUrl: "/assets/img/Default_Team_Member.png"
  }];

  private loadProgramDetails: boolean = false;
  private navigationStartSubscription;
  private team = this.onshore;
  private activeTeam = 'onshore';
  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.team;
    this.navigationStartSubscription = this.router.events.subscribe((events) => {
      if (events instanceof NavigationStart) {
        if (events.url === '/home' || events.url === '/programs') {
          this.loadProgramDetails = false;
        } else {
          this.loadProgramDetails = true;
        }
      }
    });
    this.initServiceData();
    this.tableData['totals'] = {
      "totalOffShore": this.result.totalOffShore,
      "totalOnShore": this.result.totalOnShore,
      "totalSpendings": this.result.totalSpending,
      "totalSavings": this.result.totalProgramSaving,
    }
  }

  initServiceData(): void {
    this.activatedRoute.data.subscribe((data: any) => {
      console.log(data.data);
      data.data.forEach(element => {
        this.calculate.calculateTotals(element);
        let tableDataModel = new TableDataModel();
        element.forEach((value: String, key: String) => {
          switch (key) {
            case Constants.PROGRAM_NAME: tableDataModel.programName = value;
              break;
            case Constants.ORGANISATION: tableDataModel.org = value;
              break;
            case Constants.OFF_SHORE_RESOURCE: tableDataModel.noOfOffResource = parseInt(value.toString());
              break;
            case Constants.ON_SHORE_RESOURCE: tableDataModel.noOfOnResource = parseInt(value.toString());
              break;
            case Constants.SPENDING: tableDataModel.programSpend = value;
              break;
            case Constants.SAVINGS: tableDataModel.programSaving = value;
              break;
            default: break;
          }
        });
        this.data.push(tableDataModel);
      });
      console.log(this.data);
      this.tableData['data'] = this.data;
      console.log(this.tableData);
    });
  }

  getProgramDetails(program: any) {
    this.router.navigate([`programs/programDetail`, program]);
  }

  ngOnDestroy() {
    if (this.navigationStartSubscription) {
      this.navigationStartSubscription.unsubscribe();
    }
  }
  renderTeam(evt, tabId) {
    evt.preventDefault();
    this.activeTeam = tabId;
    if (tabId === 'onshore') {
      this.team = this.onshore;
    } else {
      this.team = this.offshore;
    }
  }
}
