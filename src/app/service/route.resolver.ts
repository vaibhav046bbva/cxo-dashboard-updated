
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
    Router, Resolve, RouterStateSnapshot,
    ActivatedRouteSnapshot
} from '@angular/router';

import { GapiSession } from './gapi.session';

@Injectable()
export class RouteResolver implements Resolve<any> {
    constructor(private appService: GapiSession, private router: Router) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {

        return this.appService.getAllSheetData('Programs');
    }
}
