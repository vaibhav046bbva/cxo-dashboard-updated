import { Injectable, EventEmitter } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from "rxjs";

declare const gapi;
@Injectable()
export class GapiSession {
    result: any;
    googleAuth: gapi.auth2.GoogleAuth;
    private authHeader: Headers = new Headers();
    constructor(private http: HttpClient) { }

    initClient() {
        console.log('init client..');
        return new Promise((resolve, reject) => {
            gapi.load('client:auth2', () => {
                return gapi.client.init({
                    apiKey: environment.API_KEY,
                    clientId: environment.CLIENT_ID,
                    discoveryDocs: environment.DISCOVERY_DOCS,
                    scope: environment.SCOPES
                }).then(() => {
                    console.log('authenticated');
                    this.googleAuth = gapi.auth2.getAuthInstance();
                    resolve();
                });
            });
        });

    }

    signIn() {
        return this.googleAuth.signIn({
            prompt: 'consent'
        }).then((googleUser: gapi.auth2.GoogleUser) => {
            console.log('user profile =======', googleUser.getBasicProfile());
            //this.readFile
        });
    }

    // readFile() {
    //     console.log("results :::", this.getAllSheetData('Programs').subscribe(res => this.result = res));
    //     this.getAllSheetData('Resource');
    // }

    public getAllSheetData(range): Observable<any> {
        return Observable.create((observer: any) => {
            let promise = new Promise((resolve, reject) => {
                gapi.client.load('sheets', 'v4', () => {
                    gapi.client.sheets.spreadsheets.values.batchGet({
                        spreadsheetId: environment.sheetID,
                        ranges: range
                    }).then((resp) => {
                        console.log(resp);
                        let data = resp.result.valueRanges[0].values;
                        let dataMap = [];
                        data.forEach((element) => {
                            if (data.indexOf(element) > 0) {
                                let row = new Map();
                                element.forEach((attr) => {
                                    row.set(data[0][element.indexOf(attr)], attr);
                                });
                                dataMap.push(row);
                            }
                        });
                        console.log('data =======', dataMap);
                        resolve(dataMap);
                        observer.next(dataMap);
                        observer.complete();
                    });
                });
            });
        });
    }

    get isSignedIn(): boolean {
        return this.googleAuth.isSignedIn.get();
    }

    signOut(): void {
        this.googleAuth.signOut();
    }
}