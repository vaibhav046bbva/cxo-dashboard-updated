export class TableDataModel {
    public programName: String = '';
    public org: String = '';
    public noOfOffResource: number = 0;
    public noOfOnResource: number = 0;
    public programSpend: String = '';
    public programSaving: String = '';
}