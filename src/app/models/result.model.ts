
export class ResultModel {
    private static instance: ResultModel;
    private _totalPrograms: number;
    private _totalOffShore: number;
    private _totalOnShore: number;
    private _totalSpending: number;
    private _totalProgramSaving: number;
    static getInstance() {
        if (!ResultModel.instance) {
            ResultModel.instance = new ResultModel();
            ResultModel.instance._totalOffShore = 0;
            ResultModel.instance._totalPrograms = 0;
            ResultModel.instance._totalSpending = 0;
            ResultModel.instance._totalOnShore = 0;
            ResultModel.instance._totalProgramSaving = 0;
        }
        return ResultModel.instance;
    }
    get totalPrograms(): number {
        return this._totalPrograms;
    }
    set totalPrograms(totalPrograms) {
        this._totalPrograms = totalPrograms;
    }
    get totalOffShore(): number {
        return this._totalOffShore;
    }
    set totalOffShore(totalOffShore) {
        this._totalOffShore = totalOffShore;
    }
    get totalSpending(): number {
        return this._totalSpending;
    }
    set totalSpending(totalSpending) {
        this._totalSpending = totalSpending;
    }
    get totalProgramSaving(): number {
        return this._totalProgramSaving;
    }
    set totalProgramSaving(totalProgramSaving) {
        this._totalProgramSaving = totalProgramSaving;
    }
    get totalOnShore(): number {
        return this._totalOnShore;
    }
    set totalOnShore(totalOnShore) {
        this._totalOnShore = totalOnShore;
    }
}