import { Component, OnInit } from '@angular/core';
import { ResultModel } from '../models/result.model';
import { ActivatedRoute } from '@angular/router';
import { Calculate } from '../utils/calculate';

@Component({
    selector: 'app-home-component',
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

    data: any;
    result: ResultModel = ResultModel.getInstance();
    private calculate: Calculate = new Calculate();
    constructor(private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.activatedRoute.data.subscribe((data: any) => {
            this.data = data.data;
            this.data.forEach(element => {
                this.calculate.calculateTotals(element, true);
            });
            this.result.totalPrograms = this.data.length;
        });
    }
}
