import { Component } from "@angular/core";
import { GapiSession } from "../service/gapi.session";
import { Router } from "@angular/router";

@Component({
    selector: "signin",
    templateUrl: "./signin.component.html",
    styleUrls: ["./signin.component.css"]
})
export class SignInComponent {

    constructor(
        private appService: GapiSession,
        private router: Router
    ) {

    }

    signIn() {
        this.appService.signIn()
            .then(() => {
                if (this.appService.isSignedIn) {
                    this.router.navigate(["/home"]);
                }
            });
    }


}