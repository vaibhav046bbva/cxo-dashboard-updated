import { ResultModel } from '../models/result.model';
import { Constants } from './constants';

export class Calculate {

    result: ResultModel = ResultModel.getInstance();
    calculateTotals(element: any, remove: boolean = false): void {
        if (element.has(Constants.OFF_SHORE_RESOURCE)) {
            this.result.totalOffShore += Number(element.get(Constants.OFF_SHORE_RESOURCE));
        }
        if (element.has(Constants.ON_SHORE_RESOURCE)) {
            this.result.totalOnShore += Number(element.get(Constants.ON_SHORE_RESOURCE));
        }
        let spending = element.get(Constants.SPENDING);
        let saving = element.get(Constants.SAVINGS);
        if (remove) {
            if (spending)
                this.result.totalSpending = Number(spending.replace(/[^\d.-]/g, ''));
            if (saving)
                this.result.totalProgramSaving += Number(saving.replace(/[^\d.-]/g, ''));
        }
        else {
            if (spending)
                this.result.totalSpending = spending;
            if (saving)
                this.result.totalProgramSaving = saving;
        }
    }
}