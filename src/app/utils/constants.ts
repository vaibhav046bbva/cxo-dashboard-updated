export class Constants {
    static PROGRAM_NAME: String = 'Program Name';
    static ORGANISATION: String = 'Org';
    static OFF_SHORE_RESOURCE: String = 'No. of offshore resources';
    static ON_SHORE_RESOURCE: String = 'No. of onshore resources';
    static SPENDING: String = ' Program spend ';
    static SAVINGS: String = ' Program savings ';
}