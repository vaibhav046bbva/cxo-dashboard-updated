// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
    production: false,
    CLIENT_ID: "16075763880-n1eai5gsbh9g44qkiphsqqoo9rmq0s9n.apps.googleusercontent.com",
    API_KEY: "AIzaSyDG982cV31UNAa3yC7pmgdWlXKZbk6Hx7k",
    DISCOVERY_DOCS: ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"],
    SCOPES: 'https://www.googleapis.com/auth/drive',
    googleSheet: "https://sheets.googleapis.com/v4/spreadsheets/",
    sheetID: '1QAHd20GN9EuI8BW-Wt0T3t6Vlt2oe0-Lbjls2fUXVbU'
};
